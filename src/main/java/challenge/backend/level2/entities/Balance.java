package challenge.backend.level2.entities;

import java.util.Objects;

public class Balance {
	
	private int wallet_id;
	private int amount;
	
	public Balance() {
		super();
		this.wallet_id = 0;
		this.amount = 0;
	}
	
	public Balance(int wallet_id, int amount) {
		super();
		this.wallet_id = wallet_id;
		this.amount = amount;
	}
	
	public int getWallet_id() {
		return wallet_id;
	}
	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "Balance [wallet_id=" + wallet_id + ", amount=" + amount + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Balance b = (Balance) obj;
        return wallet_id == b.wallet_id && amount == b.amount;
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(wallet_id, amount);
	}

}
