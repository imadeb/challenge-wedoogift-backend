package challenge.backend.level2.entities;

import java.util.List;

public class DataIn {
	
	private List<Wallet> wallets;
	private List<Company> companies;
	private List<User> users;
	private List<Distribution> distributions;
	
	public List<Wallet> getWallets() {
		return wallets;
	}
	public void setWallets(List<Wallet> wallets) {
		this.wallets = wallets;
	}
	public List<Company> getCompanies() {
		return companies;
	}
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public List<Distribution> getDistributions() {
		return distributions;
	}
	public void setDistributions(List<Distribution> distributions) {
		this.distributions = distributions;
	}
	@Override
	public String toString() {
		return "DataBase [wallets=" + wallets + ", companies=" + companies + ", users=" + users + ", distributions="
				+ distributions + "]";
	}

}
