package challenge.backend.level2.entities;

import java.util.Objects;

public class Distribution {
	
	private int id;
	private int wallet_id;
	private int amount;
	private String start_date;
	private String end_date;
	private int company_id;
	private int user_id;
	
	public Distribution() {
		super();
		this.id = 0;
		this.wallet_id = 0;
		this.amount = 0;
		this.start_date = null;
		this.end_date = null;
		this.company_id = 0;
		this.user_id = 0;
	}

	public Distribution(int id, int wallet_id, int amount, String start_date, String end_date, int company_id,
			int user_id) {
		super();
		this.id = id;
		this.wallet_id = wallet_id;
		this.amount = amount;
		this.start_date = start_date;
		this.end_date = end_date;
		this.company_id = company_id;
		this.user_id = user_id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWallet_id() {
		return wallet_id;
	}
	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	
	@Override
	public String toString() {
		return "Distribution [id=" + id + ", wallet_id=" + wallet_id + ", amount=" + amount + ", start_date="
				+ start_date + ", end_date=" + end_date + ", company_id=" + company_id + ", user_id=" + user_id + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Distribution d = (Distribution) obj;
        return amount == d.amount && company_id == d.company_id && user_id == d.user_id &&
                Objects.equals(start_date, d.start_date) && Objects.equals(end_date, d.end_date);
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(amount, company_id, user_id, start_date, end_date);
	}

}
