package challenge.backend.level2.entities;

import java.util.Objects;

public class Wallet {

	private int id;
	private String name;
	private String type;
	
	public Wallet(int id, String name, String type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Wallet [id=" + id + ", name=" + name + ", type=" + type + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Wallet w = (Wallet) obj;
        return id == w.id && Objects.equals(name, w.name) && Objects.equals(type, w.type);
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(id, name, type);
	}
}
