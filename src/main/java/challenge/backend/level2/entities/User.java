package challenge.backend.level2.entities;

import java.util.List;
import java.util.Objects;

public class User {

	private int id;
	private List<Balance> balance;
	
	public User(int id, List<Balance> balance) {
		super();
		this.id = id;
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Balance> getBalance() {
		return balance;
	}
	public void setBalance(List<Balance> balance) {
		this.balance = balance;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", balance=" + balance + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User u = (User) obj;
        return id == u.id && ((balance==null && u.balance==null) || (balance!=null && u.balance!=null && balance.equals(u.balance)));
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(id, balance);
	}
	
}
