package challenge.backend.level2;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import challenge.backend.level2.entities.Balance;
import challenge.backend.level2.entities.Company;
import challenge.backend.level2.entities.DataIn;
import challenge.backend.level2.entities.DataOut;
import challenge.backend.level2.entities.Distribution;
import challenge.backend.level2.entities.User;
import challenge.backend.level2.entities.Wallet;
import challenge.backend.utils.Utils;

@Component
public class ChallengeLevel2 {

	private DataIn data = null;

	private DataIn getData() {
		if(data==null) {
			data = loadData();
		}
		return data;
	}

	/**
	 * Method loadData
	 * @return data as DataIn, which is the initial data loaded from json file
	 */
	public DataIn loadData() {
		Utils util = new Utils();
		String fileName = "level2-data/input.json";
		String jsonContent = util.getFileContent(fileName);

		Gson gson = new Gson();
		data = gson.fromJson(jsonContent, DataIn.class);

		return data;
	}

	/**
	 * Method checkCompanyBalance
	 * @param company as Company
	 * @param giftCardAmount as Integer
	 * @return Boolean, true if the company balance is greater or equal to the gift card amount
	 */
	public boolean checkCompanyBalance(Company company, int giftCardAmount) {
		boolean checkBalance = false;

		if(company!=null && company.getBalance() > 0 && company.getBalance() >= giftCardAmount) {
			checkBalance = true;
		}

		return checkBalance;
	}

	/**
	 * Method calculateUserBalance calculate the user balance with the given gift card amount
	 * @param company as Company
	 * @param user as User
	 * @param wallet as Wallet
	 * @param giftCardAmount as Integer
	 * @return Boolean if the transaction between the company balance and user balance is done correctly or not
	 */
	public boolean calculateUserBalance(Company company, User user, Wallet wallet, int giftCardAmount) {
		boolean calculated = false;
		if(user!=null && company!=null && checkCompanyBalance(company, giftCardAmount)) {

			Balance newBalance = null;
			List<Balance> newListBalance = new ArrayList<>();
			for(Balance b : user.getBalance()) {
				if(b.getWallet_id() == wallet.getId()) {

					int newAmount = b.getAmount() + giftCardAmount;
					newBalance = new Balance(wallet.getId(), newAmount);

				}else {
					newListBalance.add(b);
				}
			}

			if(newBalance!=null) {
				newListBalance.add(newBalance);
			}else {
				newBalance = new Balance(wallet.getId(), giftCardAmount);
				newListBalance.add(newBalance);
			}

			user.setBalance(newListBalance);
			company.setBalance(company.getBalance()-giftCardAmount);

			calculated = true;
		}
		return calculated;
	}

	/**
	 * Method distribute create a the transaction object to be added to the output file
	 * @param id as Integer
	 * @param walletId as Ineteger
	 * @param giftCardAmount as Integer
	 * @param startDate as String
	 * @param endDate as String
	 * @param companyId as Integer
	 * @param userId as Integer
	 * @return distribute as Distribution which contain the details of the transaction between the company and user
	 */
	private Distribution distribute(int id, int walletId, int giftCardAmount, String startDate, String endDate, int companyId, int userId) {
		Distribution dist = new Distribution();

		dist.setId(id);
		dist.setWallet_id(walletId);
		dist.setAmount(giftCardAmount);
		dist.setStart_date(startDate);
		dist.setEnd_date(endDate);
		dist.setCompany_id(companyId);
		dist.setUser_id(userId);

		return dist;
	}

	/**
	 * Method distributeGiftCards to distribute gift cards from 2 companies to 3 users
	 * @return data as DataIn, which contain the data that will be saved into json file
	 */
	public DataIn distributeGiftCards() {

		//Get lists from data object
		List<Wallet> wallets = getData().getWallets();
		List<Distribution> distributions = getData().getDistributions();
		List<Company> companies = getData().getCompanies();
		List<User> users = getData().getUsers();

		if(wallets!=null && !wallets.isEmpty()) {
			//Filter by wallet
			Wallet walletOne = wallets.stream()
					.filter(x -> x.getId() == 1)
					.collect(Utils.toSingleton());

			if(companies!=null && !companies.isEmpty()) {

				//Filter by company
				Company companyOne = companies.stream()
						.filter(x -> x.getId() == 1)
						.collect(Utils.toSingleton());

				Company companyTwo = companies.stream()
						.filter(x -> x.getId() == 2)
						.collect(Utils.toSingleton());

				if(companyOne!=null) {

					if(users!=null && !users.isEmpty()) {
						//Filter by user
						users.forEach(user -> {
							if(user.getId()==1) {
								int giftCardAmount = 50;
								if(calculateUserBalance(companyOne, user, walletOne, giftCardAmount)) {

									String startDate = "2020-09-16";
									try {
										Distribution dist = distribute(1, walletOne.getId(), giftCardAmount, startDate, Utils.addYearToDate(startDate), companyOne.getId(), user.getId());

										distributions.add(dist);
									}catch(Exception ex) {
										System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
									}
								}
							}
							if(user.getId()==2) {
								int giftCardAmount = 100;
								if(calculateUserBalance(companyOne, user, walletOne, giftCardAmount)) {

									String startDate = "2020-08-01";

									try {
										Distribution dist = distribute(2, walletOne.getId(), giftCardAmount, startDate, Utils.addYearToDate(startDate), companyOne.getId(), user.getId());

										distributions.add(dist);
									}catch(Exception ex) {
										System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
									}
								}
							}
						});
					}
				}

				if(companyTwo!=null) {

					if(users!=null && !users.isEmpty()) {
						//Filter by user
						users.forEach(user -> {
							if(user.getId()==3) {
								int giftCardAmount = 1000;
								if(calculateUserBalance(companyTwo, user, walletOne, giftCardAmount)) {

									String startDate = "2020-05-01";

									try {
										Distribution dist = distribute(3, walletOne.getId(), giftCardAmount, startDate, Utils.addYearToDate(startDate), companyTwo.getId(), user.getId());

										distributions.add(dist);
									}catch(Exception ex) {
										System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
									}
								}
							}
						});
					}
				}

				//Update data object
				getData().setCompanies(companies);
				getData().setUsers(users);
				getData().setDistributions(distributions);

			}
		}

		return getData();
	}

	/**
	 * Method distributeMealVouchers to distribute meal vouchers from 1 companies to 1 users
	 * @return data as DataOut, which contain the data that will be saved into json file
	 */
	public DataOut distributeMealVouchers() {
		
		DataOut dataOut = new DataOut();

		//Get lists from data object
		List<Wallet> wallets = getData().getWallets();
		List<Distribution> distributions = getData().getDistributions();
		List<Company> companies = getData().getCompanies();
		List<User> users = getData().getUsers();

		if(wallets!=null && !wallets.isEmpty() && companies!=null && !companies.isEmpty() && users!=null && !users.isEmpty()) {

			//Filter by wallet
			Wallet walletTwo = wallets.stream()
					.filter(x -> x.getId() == 2)
					.collect(Utils.toSingleton());

			//Filter by company
			Company companyOne = companies.stream()
					.filter(x -> x.getId() == 1)
					.collect(Utils.toSingleton());

			//Filter by user
			User userOne = users.stream()
					.filter(x -> x.getId() == 1)
					.collect(Utils.toSingleton());

			int giftCardAmount = 250;
			if(calculateUserBalance(companyOne, userOne, walletTwo, giftCardAmount)) {

				String startDate = "2020-05-01";
				try {
					Distribution dist = distribute(4, walletTwo.getId(), giftCardAmount, startDate, Utils.addMonthsToDateUntilFebruary(startDate), companyOne.getId(), userOne.getId());

					distributions.add(dist);
				}catch(Exception ex) {
					System.err.println("Can not distribute a gift carde for the user: "+userOne.getId()+", technical error occurred regarding the validity dates !");
				}

			}

			//Create data output object
			dataOut.setCompanies(companies);
			dataOut.setUsers(users);
			dataOut.setDistributions(distributions);

			//Convert data object to json
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String jsonContent = gson.toJson(dataOut, DataOut.class);

			//Write json output to json file
			Utils.setContentToFile("level2-data-output.json", jsonContent);

		}

		return dataOut;

	}


}
