package challenge.backend.level1.entities;

import java.util.Objects;

public class Company {

	private int id;
	private String name;
	private int balance;
	
	public Company(int id, String name, int balance) {
		super();
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", balance=" + balance + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Company c = (Company) obj;
        return id == c.id && balance == c.balance && Objects.equals(name, c.name);
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(id, name, balance);
	}
}
