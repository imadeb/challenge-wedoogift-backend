package challenge.backend.level1.entities;

import java.util.Objects;

public class User {

	private int id;
	private int balance;
	
	public User(int id, int balance) {
		super();
		this.id = id;
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", balance=" + balance + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User u = (User) obj;
        return id == u.id && balance == u.balance;
	}
	
	@Override
	public int hashCode() {
		 return Objects.hash(id, balance);
	}
}
