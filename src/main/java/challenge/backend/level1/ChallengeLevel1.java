package challenge.backend.level1;

import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import challenge.backend.level1.entities.Company;
import challenge.backend.level1.entities.DataInOut;
import challenge.backend.level1.entities.Distribution;
import challenge.backend.level1.entities.User;
import challenge.backend.utils.Utils;

@Component
public class ChallengeLevel1 {

	private DataInOut data = null;

	private DataInOut getData() {
		if(data==null) {
			data = loadData();
		}
		return data;
	}

	/**
	 * Method loadData
	 * @return data as DataInOut, which is the initial data loaded from json file
	 */
	public DataInOut loadData() {
		Utils util = new Utils();
		String fileName = "level1-data/input.json";
		String jsonContent = util.getFileContent(fileName);

		Gson gson = new Gson();
		data = gson.fromJson(jsonContent, DataInOut.class);

		return data;
	}

	/**
	 * Method checkCompanyBalance
	 * @param company as Company
	 * @param giftCardAmount as Integer
	 * @return Boolean, true if the company balance is greater or equal to the gift card amount
	 */
	public boolean checkCompanyBalance(Company company, int giftCardAmount) {
		boolean checkBalance = false;

		if(company!=null && company.getBalance() > 0 && company.getBalance() >= giftCardAmount) {
			checkBalance = true;
		}

		return checkBalance;
	}

	/**
	 * Method calculateUserBalance calculate the user balance with the given gift card amount
	 * @param company as Company
	 * @param user as User
	 * @param giftCardAmount as Integer
	 * @return Boolean if the transaction between the company balance and user balance is done correctly or not
	 */
	public boolean calculateUserBalance(Company company, User user, int giftCardAmount) {
		boolean calculatedAmount = false;
		if(user!=null && company!=null && checkCompanyBalance(company, giftCardAmount)) {
			user.setBalance(user.getBalance()+giftCardAmount);
			company.setBalance(company.getBalance()-giftCardAmount);

			calculatedAmount = true;
		}
		return calculatedAmount;
	}

	/**
	 * Method distribute create a the transaction object to be added to the output file
	 * @param id as Integer
	 * @param giftCardAmount as Integer
	 * @param startDate as String
	 * @param endDate as String
	 * @param companyId as Integer
	 * @param userId as Integer
	 * @return distribute as Distribution which contain the details of the transaction between the company and user
	 */
	private Distribution distribute(int id, int giftCardAmount, String startDate, String endDate, int companyId, int userId) {
		Distribution dist = new Distribution();

		dist.setId(id);
		dist.setAmount(giftCardAmount);
		dist.setStart_date(startDate);
		dist.setEnd_date(endDate);
		dist.setCompany_id(companyId);
		dist.setUser_id(userId);

		return dist;
	}

	/**
	 * Method distributeGiftCards to distribute gift cards from 2 companies to 3 users
	 * @return data as DataInOut, which contain the data that will be saved into json file
	 */
	public DataInOut distributeGiftCards() {

		//Get lists from data object
		List<Distribution> distributions = getData().getDistributions();
		List<Company> companies = getData().getCompanies();
		List<User> users = getData().getUsers();

		if(companies!=null && !companies.isEmpty()) {

			//Filter by company
			Company companyOne = companies.stream()
					.filter(x -> x.getId() == 1)
					.collect(Utils.toSingleton());

			Company companyTwo = companies.stream()
					.filter(x -> x.getId() == 2)
					.collect(Utils.toSingleton());

			if(companyOne!=null) {

				if(users!=null && !users.isEmpty()) {
					//Filter by user
					users.forEach(user -> {
						if(user.getId()==1) {
							int giftCardAmount = 50;
							if(calculateUserBalance(companyOne, user, giftCardAmount)) {

								String startDate = "2020-09-16";
								try {
									Distribution dist = distribute(1, giftCardAmount, startDate, Utils.addYearToDate(startDate), companyOne.getId(), user.getId());

									distributions.add(dist);
								}catch(Exception ex) {
									System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
								}
							}
						}

						if(user.getId()==2) {
							int giftCardAmount = 100;
							if(calculateUserBalance(companyOne, user, giftCardAmount)) {

								String startDate = "2020-08-01";
								try {
									Distribution dist = distribute(2, giftCardAmount, startDate, Utils.addYearToDate(startDate), companyOne.getId(), user.getId());

									distributions.add(dist);
								}catch(Exception ex) {
									System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
								}
							}
						}
					});
				}
			}

			if(companyTwo!=null) {
				if(users!=null && !users.isEmpty()) {
					//Filter by user
					users.forEach(user -> {
						if(user.getId()==3) {
							int giftCardAmount = 1000;
							if(calculateUserBalance(companyTwo, user, giftCardAmount)) {

								String startDate = "2020-05-01";
								try {
									Distribution dist = distribute(3, giftCardAmount, startDate, Utils.addYearToDate(startDate), companyTwo.getId(), user.getId());

									distributions.add(dist);
								}catch(Exception ex) {
									System.err.println("Can not distribute a gift carde for the user: "+user.getId()+", technical error occurred regarding the validity dates !");
								}
							}
						}
					});
				}
			}

			//Update data object
			getData().setCompanies(companies);
			getData().setUsers(users);
			getData().setDistributions(distributions);

			//Convert data object to json
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String jsonContent = gson.toJson(getData(), DataInOut.class);

			//Write json output to json file
			Utils.setContentToFile("level1-data-output.json", jsonContent);

		}

		return getData();
	}

}
