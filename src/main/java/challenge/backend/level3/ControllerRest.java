package challenge.backend.level3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import challenge.backend.level1.ChallengeLevel1;
import challenge.backend.level2.ChallengeLevel2;

@RestController
@RequestMapping(value="challenge") 
public class ControllerRest {

	@Autowired
	private ChallengeLevel1 level1;
	
	@Autowired
	private ChallengeLevel2 level2;

	@GetMapping(value = "/level1/distributeGiftCards")
	public challenge.backend.level1.entities.DataInOut level1DistributeGiftCards() {
		return level1.distributeGiftCards();
	}
	
	@GetMapping(value = "/level2/distributeMealVouchers")
	public challenge.backend.level2.entities.DataOut level2DistributeMealVouchers() {
		level2.distributeGiftCards();
		return level2.distributeMealVouchers();
	}

}
