package challenge.backend.level3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"challenge.backend"})
@SpringBootApplication
public class ChallengeApplication {

    // start everything
    public static void main(String[] args) {
        SpringApplication.run(ChallengeApplication.class, args);
    }

}