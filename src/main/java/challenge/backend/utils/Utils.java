package challenge.backend.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class Utils {

	/**
	 * Method addYearToDate
	 * @param date as String
	 * @return String, returning adding one year to the given date
	 */
	public static String addYearToDate(String date) {
		return LocalDate
				.parse(date)
				.plusYears(1)
				.minusDays(1)
				.toString();
	}

	/**
	 * Method addMonthsToDateUntilFebruary to add period of time between the given date and the end of February of the following year
	 * @param date as String
	 * @return String, returning adding months until end of February to the given date
	 */
	public static String addMonthsToDateUntilFebruary(String date) {

		//Parse date and throw exception if possible to stop the process
		LocalDate givenDate = LocalDate.parse(date);

		//Add one year as expiry year
		int expiryYear = givenDate.plusYears(1).getYear();

		//Set February as expiry month
		LocalDate expiryDate = LocalDate.of(expiryYear, Month.FEBRUARY, 28);

		//Adjust to years with February 29
		expiryDate = expiryDate.with(TemporalAdjusters.lastDayOfMonth());

		//Return the date as YYYY-MM-DD
		return expiryDate.toString();
	}

	/**
	 * Method getFileContent to load a file from resources
	 * @param fileName as String
	 * @return String with the file content
	 */
	public String getFileContent(String fileName) {
		StringBuilder content = new StringBuilder();
		InputStream is = getFileFromResourceAsStream(fileName);
		try (InputStreamReader streamReader =
				new InputStreamReader(is, StandardCharsets.UTF_8);
				BufferedReader reader = new BufferedReader(streamReader)) {

			String line;
			while ((line = reader.readLine()) != null) {
				content.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}

	// get a file from the resources folder
	// works everywhere, IDEA, unit test and JAR file.
	private InputStream getFileFromResourceAsStream(String fileName) {

		// The class loader that loaded the class
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(fileName);

		// the stream holding the file content
		if (inputStream == null) {
			throw new IllegalArgumentException("file not found! " + fileName);
		} else {
			return inputStream;
		}

	}

	/**
	 * Method setContentToFile to write content into output file
	 * @param fileName as String
	 * @param content as String
	 */
	public static void setContentToFile(String fileName, String content) {

		try {
			String filePath = "c:/wedootest-debbour/";
			Files.createDirectories(Paths.get(filePath));
			// Java 11 , default StandardCharsets.UTF_8
			Files.writeString(Paths.get(filePath + fileName), content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method toSingleton to return one object from list after filtering
	 * @return Object<T> the only object exists in the list
	 */
	public static <T> Collector<T, ?, T> toSingleton() {
		return Collectors.collectingAndThen(
				Collectors.toList(),
				list -> {
					if (list.size() != 1) {
						throw new IllegalStateException();
					}
					return list.get(0);
				}
				);
	}


}
