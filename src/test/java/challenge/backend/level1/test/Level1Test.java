package challenge.backend.level1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import challenge.backend.level1.ChallengeLevel1;
import challenge.backend.level1.entities.Company;
import challenge.backend.level1.entities.DataInOut;
import challenge.backend.level1.entities.Distribution;
import challenge.backend.level1.entities.User;

class Level1Test {

	private static Instant startedAt;
	private ChallengeLevel1 level1;
	private DataInOut data;

	@BeforeEach
	void init() {
		level1 = new ChallengeLevel1();
		data = level1.loadData();
	}

	@AfterEach
	void reset() {
		level1 = null;
	}

	@Test
	void testLoadData() {
		//Arrange

		//Act
		List<Company> companies = data.getCompanies();
		List<User> users = data.getUsers();
		List<Distribution> distributions = data.getDistributions();

		//Assert
		assertNotNull(companies);
		assertNotNull(users);
		assertNotNull(distributions);

		assertEquals(false, companies.isEmpty());
		assertEquals(false, users.isEmpty());
		assertEquals(true, distributions.isEmpty());

		//Test equals
		assertThat(companies, hasItems(
				new Company(1, "Wedoogift", 1000),
				new Company(2, "Wedoofood", 3000)
				));

		//Test class property, and its value
		assertThat(companies, containsInAnyOrder(
				hasProperty("name", is("Wedoogift")),
				hasProperty("name", is("Wedoofood"))
				));

		assertThat(users, hasItems(
				new User(1, 100),
				new User(2, 0),
				new User(3, 0)
				));

	}

	@Test
	void testCheckCompanyBalance() {
		//Arrange
		int giftCardAmount = 100;
		Company c = new Company(1, "Test", 100);

		//Act
		boolean checkResult = level1.checkCompanyBalance(c, giftCardAmount);

		//Assert
		assertEquals(true, checkResult);
	}

	@Test
	void testCalculateUserBalance() {
		//Arrange
		int giftCardAmount = 50;
		Company c = new Company(1, "Test", 100);
		User u = new User(1, 0);

		//Act
		boolean checkResult = level1.calculateUserBalance(c, u, giftCardAmount);

		//Assert
		assertEquals(true, checkResult);
	}

	@Test
	void testDistributeGiftCards() {
		//Arrange

		//Act
		data = level1.distributeGiftCards();
		
		List<Company> companies = data.getCompanies();
		List<User> users = data.getUsers();
		List<Distribution> distributions = data.getDistributions();

		//Assert
		assertNotNull(companies);
		assertNotNull(users);
		assertNotNull(distributions);

		assertEquals(false, companies.isEmpty());
		assertEquals(false, users.isEmpty());
		assertEquals(false, distributions.isEmpty());

		//Test equals
		assertThat(companies, containsInAnyOrder(
				new Company(1, "Wedoogift", 850),
				new Company(2, "Wedoofood", 2000)
				));

		assertThat(users, containsInAnyOrder(
				new User(1, 150),
				new User(2, 100),
				new User(3, 1000)
				));

		assertThat(distributions, containsInAnyOrder(
				new Distribution(1, 50, "2020-09-16", "2021-09-15", 1, 1),
				new Distribution(2, 100, "2020-08-01", "2021-07-31", 1, 2),
				new Distribution(3, 1000, "2020-05-01", "2021-04-30", 2, 3)
				));

	}

	@BeforeAll
	static void initStartingTime() {
		startedAt = Instant.now();
	}

	@AfterAll
	static void showTestDuration() {
		Instant endedAt = Instant.now();
		long duration = Duration.between(startedAt, endedAt).toMillis();
		System.out.println(MessageFormat.format("Tests duration : {0} ms", duration));
	}
}
