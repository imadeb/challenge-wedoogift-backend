package challenge.backend.level3.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ControllerRestTest {

	@Autowired
	private MockMvc mockMvc;

	@WithMockUser("USER")
	@Test
	public void test_level1_distributeGiftCards() throws Exception {

		mockMvc.perform(get("/challenge/level1/distributeGiftCards"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.companies[0].id", is(1)))
		.andExpect(jsonPath("$.companies[0].name", is("Wedoogift")))
		.andExpect(jsonPath("$.companies[0].balance", is(850)))
		.andExpect(jsonPath("$.companies[1].id", is(2)))
		.andExpect(jsonPath("$.companies[1].name", is("Wedoofood")))
		.andExpect(jsonPath("$.companies[1].balance", is(2000)));
	}
	
	@WithMockUser("USER")
	@Test
	public void test_level2_distributeMealVouchers() throws Exception {

		mockMvc.perform(get("/challenge/level2/distributeMealVouchers"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.companies[0].id", is(1)))
		.andExpect(jsonPath("$.companies[0].name", is("Wedoogift")))
		.andExpect(jsonPath("$.companies[0].balance", is(600)))
		.andExpect(jsonPath("$.companies[1].id", is(2)))
		.andExpect(jsonPath("$.companies[1].name", is("Wedoofood")))
		.andExpect(jsonPath("$.companies[1].balance", is(2000)));
	}

	@Test
	public void test_unauthorized_401() throws Exception {
		mockMvc.perform(get("/challenge/level1/distributeGiftCards"))
		.andDo(print())
		.andExpect(status().isUnauthorized());

		mockMvc.perform(get("/challenge/level2/distributeMealVouchers"))
		.andDo(print())
		.andExpect(status().isUnauthorized());
	}

}
