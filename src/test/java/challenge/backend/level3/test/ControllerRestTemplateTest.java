package challenge.backend.level3.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ControllerRestTemplateTest {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_level1_distributeGiftCards() throws Exception {

        String expected = "{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":850},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":2000}],\"users\":[{\"id\":1,\"balance\":150},{\"id\":2,\"balance\":100},{\"id\":3,\"balance\":1000}],\"distributions\":[{\"id\":1,\"amount\":50,\"start_date\":\"2020-09-16\",\"end_date\":\"2021-09-15\",\"company_id\":1,\"user_id\":1},{\"id\":2,\"amount\":100,\"start_date\":\"2020-08-01\",\"end_date\":\"2021-07-31\",\"company_id\":1,\"user_id\":2},{\"id\":3,\"amount\":1000,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-04-30\",\"company_id\":2,\"user_id\":3}]}";
        
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "password")
                .getForEntity("/challenge/level1/distributeGiftCards", String.class);

        printJSON(response);

        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JSONAssert.assertEquals(expected, response.getBody(), false);

    }
    
    @Test
    public void test_level2_distributeMealVouchers() throws Exception {

        String expected = "{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":600},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":2000}],\"users\":[{\"id\":1,\"balance\":[{\"wallet_id\":1,\"amount\":150},{\"wallet_id\":2,\"amount\":250}]},{\"id\":2,\"balance\":[{\"wallet_id\":1,\"amount\":100}]},{\"id\":3,\"balance\":[{\"wallet_id\":1,\"amount\":1000}]}],\"distributions\":[{\"id\":1,\"wallet_id\":1,\"amount\":50,\"start_date\":\"2020-09-16\",\"end_date\":\"2021-09-15\",\"company_id\":1,\"user_id\":1},{\"id\":2,\"wallet_id\":1,\"amount\":100,\"start_date\":\"2020-08-01\",\"end_date\":\"2021-07-31\",\"company_id\":1,\"user_id\":2},{\"id\":3,\"wallet_id\":1,\"amount\":1000,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-04-30\",\"company_id\":2,\"user_id\":3},{\"id\":4,\"wallet_id\":2,\"amount\":250,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-02-28\",\"company_id\":1,\"user_id\":1}]}";

        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "password")
                .getForEntity("/challenge/level2/distributeMealVouchers", String.class);

        printJSON(response);

        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JSONAssert.assertEquals(expected, response.getBody(), false);

    }

    @Test
    public void test_unauthorized_401() throws Exception {

        String expected = "{}";

        ResponseEntity<String> response = restTemplate
                .getForEntity("/challenge/level1/distributeGiftCards", String.class);

        printJSON(response);

        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());

        JSONAssert.assertEquals(expected, response.getBody(), false);

    }

    private static void printJSON(Object object) {
        String result;
        try {
            result = om.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            System.out.println(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
