package challenge.backend.level2.test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import challenge.backend.level2.ChallengeLevel2;
import challenge.backend.level2.entities.Balance;
import challenge.backend.level2.entities.Company;
import challenge.backend.level2.entities.DataIn;
import challenge.backend.level2.entities.DataOut;
import challenge.backend.level2.entities.Distribution;
import challenge.backend.level2.entities.User;
import challenge.backend.level2.entities.Wallet;

class Level2Test {

	private static Instant startedAt;
	private ChallengeLevel2 level2;
	private DataIn data;

	@BeforeEach
	void init() {
		level2 = new ChallengeLevel2();
		data = level2.loadData();
	}

	@AfterEach
	void reset() {
		level2 = null;
	}

	@Test
	void testLoadData() {
		//Arrange

		//Act
		List<Wallet> wallets = data.getWallets();
		List<Company> companies = data.getCompanies();
		List<User> users = data.getUsers();
		List<Distribution> distributions = data.getDistributions();

		//Assert
		assertNotNull(wallets);
		assertNotNull(companies);
		assertNotNull(users);
		assertNotNull(distributions);

		assertEquals(false, wallets.isEmpty());
		assertEquals(false, companies.isEmpty());
		assertEquals(false, users.isEmpty());
		assertEquals(true, distributions.isEmpty());

		//Test equals
		assertThat(companies, hasItems(
				new Company(1, "Wedoogift", 1000),
				new Company(2, "Wedoofood", 3000)
				));

		//Test class property, and its value
		assertThat(companies, containsInAnyOrder(
				hasProperty("name", is("Wedoogift")),
				hasProperty("name", is("Wedoofood"))
				));

		assertThat(users, hasItems(
				new User(1, Arrays.asList(new Balance(1, 100))),
				new User(2, new ArrayList<Balance>()),
				new User(3, new ArrayList<Balance>())
				));

		assertThat(wallets, containsInAnyOrder(
				new Wallet(1, "gift cards", "GIFT"),
				new Wallet(2, "food cards", "FOOD")
				));

	}

	@Test
	void testCheckCompanyBalance() {
		//Arrange
		int giftCardAmount = 100;
		Company c = new Company(1, "Test", 100);

		//Act
		boolean checkResult = level2.checkCompanyBalance(c, giftCardAmount);

		//Assert
		assertEquals(true, checkResult);
	}

	@Test
	void testCalculateUserBalance() {
		//Arrange
		int giftCardAmount = 50;
		Wallet w = new Wallet(1, "gift cards" , "GIFT");
		Company c = new Company(1, "Test", 100);
		User u = new User(1, Arrays.asList(new Balance(1, 0)));

		//Act
		boolean checkResult = level2.calculateUserBalance(c, u, w, giftCardAmount);

		//Assert
		assertEquals(true, checkResult);
	}

	@Test
	void testDistributeMealVouchers() {
		//Arrange

		//Act
		level2.distributeGiftCards();
		DataOut dataOut = level2.distributeMealVouchers();
		
		List<Company> companies = dataOut.getCompanies();
		List<User> users = dataOut.getUsers();
		List<Distribution> distributions = dataOut.getDistributions();

		//Assert
		assertNotNull(companies);
		assertNotNull(users);
		assertNotNull(distributions);

		assertEquals(false, companies.isEmpty());
		assertEquals(false, users.isEmpty());
		assertEquals(false, distributions.isEmpty());

		//Test equals
		assertThat(companies, containsInAnyOrder(
				new Company(1, "Wedoogift", 600),
				new Company(2, "Wedoofood", 2000)
				));

		assertThat(users, containsInAnyOrder(
				new User(1, Arrays.asList(new Balance(1, 150), new Balance(2, 250))),
				new User(2, Arrays.asList(new Balance(1, 100))),
				new User(3, Arrays.asList(new Balance(1, 1000)))
				));

		assertThat(distributions, containsInAnyOrder(
				new Distribution(1, 1, 50, "2020-09-16", "2021-09-15", 1, 1),
				new Distribution(2, 1, 100, "2020-08-01", "2021-07-31", 1, 2),
				new Distribution(3, 1, 1000, "2020-05-01", "2021-04-30", 2, 3),
				new Distribution(4, 2, 250, "2020-05-01", "2021-02-28", 1, 1)
				));

	}

	@BeforeAll
	static void initStartingTime() {
		startedAt = Instant.now();
	}

	@AfterAll
	static void showTestDuration() {
		Instant endedAt = Instant.now();
		long duration = Duration.between(startedAt, endedAt).toMillis();
		System.out.println(MessageFormat.format("Tests duration : {0} ms", duration));
	}
}
